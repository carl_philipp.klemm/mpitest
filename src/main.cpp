#include <iostream>
#include <mpi.h>
#include <assert.h>
#include <unistd.h>

#include "log.h"

int parent(const char* spawnName, size_t toSpwan)
{

	MPI_Comm intercomm;
	Log(Log::INFO)<<"Parent, will spwan "<<toSpwan<<" procs";
	std::cout<<std::flush;

	{
		int* errorCodes = new int[toSpwan];
		if(MPI_Comm_spawn(spawnName, MPI_ARGV_NULL, toSpwan, MPI_INFO_NULL, 0, MPI_COMM_SELF, &intercomm, errorCodes) != MPI_SUCCESS)
		{
			Log(Log::ERROR)<<"Unable to spawn "<<toSpwan<<" procs";
			return 1;
		}
		delete[] errorCodes;

		int numProc = 7;
		MPI_Comm_remote_size(intercomm, &numProc);
		do
		{
			MPI_Status status;
			int ret;
			Log(Log::DEBUG)<<numProc<<" processes running";
			MPI_Recv(&ret, 1, MPI_INT, MPI_ANY_SOURCE, MPI_ANY_TAG, intercomm, &status);
			Log(Log::INFO)<<"Child "<<status.MPI_SOURCE<<" is exiting status "<<ret;
			--numProc;
		} while(numProc > 0);
	}

	sleep(1); //so as to not loose final debug output
	Log(Log::INFO)<<"master exit";
	return 0;
}

int child(MPI_Comm parentComm)
{
	Log::level = Log::INFO;
	int rank;
	MPI_Comm_rank(parentComm, &rank);
	Log(Log::INFO)<<"Child Local rank "<<rank;

	sleep(2);

	int ret = 0;
	MPI_Send(&ret, 1, MPI_INT, 0, 1, parentComm);

	return ret;
}

int main(int argc, char** argv)
{
	Log::level   = Log::DEBUG;
	Log::headers = true;
	MPI_Init(&argc, &argv);

	size_t nProc = 127;

	MPI_Comm parentComm;
	if(MPI_Comm_get_parent(&parentComm) != MPI_SUCCESS)
	{
		Log(Log::ERROR)<<"Failed to get parent comm handle";
		return 1;
	}

	int ret;
	if(parentComm == MPI_COMM_NULL)
		ret = parent(argv[0], nProc);
	else
		ret = child(parentComm);

	MPI_Finalize();
	return ret;
}
